package com.restproject.entities;

import java.io.Serializable;
import java.util.RandomAccess;

/**
 * Created by Servlok on 2014-04-08.
 */
public class Greeting implements Serializable,RandomAccess {
    private final long id;
    private final String content;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
