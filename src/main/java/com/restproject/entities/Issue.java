package com.restproject.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by Servlok on 2014-05-03.
 */
@Entity
public class Issue {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long idUserReport;

    private long idUserExecute;

    private String description;

    private String name;

    private String priority;

    private Date creationDate;

    private String status;

    private Date changedDate;

    public long getIdUserReport() {
        return idUserReport;
    }

    public void setIdUserReport(long idUserReport) {
        this.idUserReport = idUserReport;
    }

    public long getIdUserExecute() {
        return idUserExecute;
    }

    public void setIdUserExecute(long idUserExecute) {
        this.idUserExecute = idUserExecute;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(Date changedDate) {
        this.changedDate = changedDate;
    }
}
