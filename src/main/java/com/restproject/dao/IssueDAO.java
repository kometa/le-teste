package com.restproject.dao;

import com.restproject.entities.Issue;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Servlok on 2014-05-03.
 */
@RepositoryRestResource(collectionResourceRel = "issue", path = "issue")
public interface IssueDAO extends PagingAndSortingRepository<Issue, Long>{
    List<Issue> findByStatus(@Param("status") String status);
    List<Issue> findByIdUserReport(@Param("id") String id);
    List<Issue> findByIdUserExecute(@Param("id") String id);
}
